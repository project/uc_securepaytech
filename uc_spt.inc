<?php
/**
 * @file
 * Common function for SecurePayTech and SecurePayTech Hosted Payment Page modules.
 */

function uc_spt_test_help() {
  $output = '<div class="uc-spt-payment-testing">';
  $output .= t('Payment testing is enabled.') . '<br/>';
  $output .= t('<table class="testing-dollar-values">
    <tr><th>Cent Value</th><th>Response</th></tr>
    <tr><td>.00</td><td>Transaction OK</td></tr>
    <tr><td>.10</td><td>Insufficient Funds</td></tr>
    <tr><td>.54</td><td>Card expired</td></tr>
    <tr><td>.57</td><td>Unsupported Transaction Type</td></tr>
    <tr><td>.75</td><td>Card Declined</td></tr>
    <tr><td>.91</td><td>Communications Error</td></tr>
  </table>
  <p>Other values may result in an unspecified error.</p>');

  if (variable_get('uc_credit_cvv_enabled', TRUE)) {
    $output .= '<table class="testing-csc-codes">
    <tr><th>Code</th><th>Response</th></tr>
    <tr><td>100</td><td>CSC matched</td></tr>
    <tr><td>102</td><td>CSC not processed</td></tr>
    <tr><td>103</td><td>CSC unsupported by card issuer (payment may be accepted)</td></tr>
    <tr><td>104</td><td>CSC not matched</td></tr>
    </table>';
  }

  $output .= t('<table class="testing-credit-card-numbers">
    <tr><th>Card Type</th><th>Card Number</th><th>Expiry (MM/YY)</th></tr>
    <tr><td>VISA</td><td>4987-6543-2109-8769</td><td>05/13</td></tr>
    <tr><td>Mastercard</td><td>5123-4567-8901-2346</td><td>05/13</td></tr>
    <tr><td>American Express</td><td>3456-7890-1234-564</td><td>05/13</td></tr>
    <tr><td>Diners Club</td><td>3012-345678-9019</td><td>05/13</td></tr>
  </table>');
  $output .='</div>';
  return $output;
}

/**
 * In test mode, specific cent options can trigger specific errors from the gateway.
 */
function uc_spt_cent_options() {
  return array(
    '00' => t('.00 Transaction OK'),
    '10' => t('.10 Insufficient Funds'),
    '54' => t('.54 Card Expired'),
    '57' => t('.57 Unsupported Transaction Type'),
    '75' => t('.75 Card Declined'),
    '91' => t('.91 Communications Error'),
  );
}

/**
 * Define SecurePayTech result codes
 * @return String Error text explaining a SecurePayTech result code.
 */
function uc_spt_result_codes($code) {
  $codes = array(
   0 => t('Could not connect to SecurePayTech host. Please call or email your order.'), // added by Jonathan.

   // ACCEPTED.
   1 => t('Transaction OK'), // Transaction OK

   // Following codes represent DECLINED.
   2 => t('Reason: Insufficient funds'),
   3 => t('Reason: Card Expired'),
   4 => t('Reason: Card is declined for a reason aside from insufficient funds or an expired card.'),
   5 => t("Reason: Server Error Occurred<br/>A software error occurred either at SecurePayTech or at one of our provider's systems."),
   6 => t('Reason: Communications Error<br/>This usually indicates that a timeout has occurred while processing the transaction request.'),
   7 => t('Reason: Unsupported Transaction Type'),
   8 => t('Reason: Bad or Malformed Request'), // This error will return a reason in the 'failReason' result field.
   9 => t('Reason: Invalid Card Number. Card number was incorrect length, or failed verification.'),
  );
  return $code ? $codes[$code] : $codes;
}

/**
 * Return array of SecurePaytech response codes for CVV (CSC) value, or a specific reponse string.
 */
function uc_spt_cvv_result_codes($code) {
  $codes = array(
    'S' => t('Not provided.'),
    'M' => t('CVV matched.'),
    'P' => t('CVV not processed.'),
    'U' => t('CVV unsupported by card issuer.'),
    'N' => t('CVV not matched.'),
    'Unsupported' => t('CVV unsupported by merchant bank.'),
  );
  return $code ? $codes[$code] : $codes;
}