/* Reveal back button, but alter form to submit to Drupal not SPT */

Drupal.behaviors.uc_spt_hpp_init = function(context) {
  $('#edit-back').show().bind('click', function(){
    $('#uc-cart-checkout-review-form').attr('action', '/cart/checkout');
  });

}